import argparse

from my_dymoprint import print_label


def pivot(sector, size):
    n = sector
    s = size
    return (
        (f'XL-S{n}-LV-L56-L',),
        (f'XL-S{n}-LV-L78-L',),
        (f'S{n} Q{s}1+Q{s}2 L6', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L8', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L5+L7', 'pFEB PT SR LV'),
        (f'XL-S{n}-LV-L56-R',),
        (f'XL-S{n}-LV-L78-R',),
        (f'S{n} Q{s}1+Q{s}2 L5', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L7', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L6+L8', 'pFEB PT SR LV',),
    )


def confirm(sector, size):
    n = sector
    s = size
    return (
        (f'XL-S{n}-LV-L12-L',),
        (f'XL-S{n}-LV-L34-L',),
        (f'S{n} Q{s}1+Q{s}2 L1', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L3', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L2+L4', 'pFEB PT SR LV',),
        (f'XL-S{n}-LV-L12-R',),
        (f'XL-S{n}-LV-L34-R',),
        (f'S{n} Q{s}1+Q{s}2 L2', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L4', 'sFEB Router SR LV',),
        (f'S{n} Q{s}1+Q{s}2 L1+L3', 'pFEB PT SR LV',),
    )


def print_labels(labels, index=0, _dry_run=False):
    try:
        for i, labels in list(enumerate(labels))[index:]:
            fsr = len(labels) / 2.2
            print(i, labels)
            if _dry_run:
                continue
            print_label(*labels, fontsizeratio=fsr)
            print_label(*labels, fontsizeratio=fsr)
    except KeyboardInterrupt:
        print()


if __name__ == "__main__":
    def main():
        parser = argparse.ArgumentParser()
        parser.add_argument('sector', type=int,
                            help='The sector number')
        parser.add_argument('small_or_large', choices=('small', 'large'),
                            help='Small or large wedge')
        parser.add_argument('pivot_or_confirm', choices=('pivot', 'confirm'),
                            help='Pivot or confirm wedge')
        parser.add_argument('offset', type=int, nargs='?', default=0,
                            help='Which label index to start at')
        parser.add_argument('--dry-run', action='store_true',
                            help=argparse.SUPPRESS)
        args = parser.parse_args()

        size = 'S' if args.small_or_large == 'small' else 'L'
        if args.pivot_or_confirm == 'pivot':
            all_labels = pivot(args.sector, size)
        else:
            all_labels = confirm(args.sector, size)

        print_labels(all_labels, args.offset, args.dry_run)

    def custom():
        n = 8
        s = 'S'
        labels = (
            (f'S{n} Q{s}1+Q{s}2 L6+L8', 'pFEB PT SR LV',),
        )
        print_labels(labels)

    main()
    # custom()
