
Cable;Length;SN;Repeater SN
# On wedge L1DDC Cables
QL3C_Layer_3_sFEB_L1DDC_1_L1DDC_L3_s-pFE3_1m;1.0m;21664;
QL2C_Layer_3_sFEB_L1DDC_1_L1DDC_L3_s-pFE2_2m;2m;21656;
QL1C_Layer_3_sFEB_L1DDC_2_L1DDC_L3_s-pFE1-2_3m;3m;21647;
QL1C_Layer_3_sFEB_L1DDC_1_L1DDC_L3_s-pFE1-1_3m;3m;21646;
QL1C_Layer_4_pFEB_L1DDC_1_L1DDC_L4_s-pFE1-1_3m;3m;21641;
QL2C_Layer_4_pFEB_L1DDC_1_L1DDC_L4_s-pFE2_1.5m;1.5m;21653;
QL3C_Layer_4_pFEB_L1DDC_1_L1DDC_L4_s-pFE3_1m;1.0m;21661;
QL3C_Layer_1_sFEB_L1DDC_1_L1DDC_L1_s-pFE3_1m;1.0m;21662;
QL2C_Layer_1_sFEB_L1DDC_1_L1DDC_L1_s-pFE2_2m;2m;21654;
QL1C_Layer_1_sFEB_L1DDC_2_L1DDC_L1_s-pFE1-2_3m;3m;21643;
QL1C_Layer_1_sFEB_L1DDC_1_L1DDC_L1_s-pFE1-1_3m;3m;21642;
QL1C_Layer_2_pFEB_L1DDC_1_L1DDC_L2_s-pFE1-1_3m;3m;21639;
QL2C_Layer_2_pFEB_L1DDC_1_L1DDC_L2_s-pFE2_1.5m;1.5m;21651;
QL3C_Layer_2_pFEB_L1DDC_1_L1DDC_L2_s-pFE3_1m;1.0m;21659;
QL3C_Layer_2_sFEB_L1DDC_1_L1DDC_L2_s-pFE3_1m;1.0m;21663;
QL2C_Layer_2_sFEB_L1DDC_1_L1DDC_L2_s-pFE2_2m;2m;21655;
QL1C_Layer_2_sFEB_L1DDC_2_L1DDC_L2_s-pFE1-2_3m;3m;21645;
QL1C_Layer_2_sFEB_L1DDC_1_L1DDC_L2_s-pFE1-1_3m;3m;21644;
QL1C_Layer_1_pFEB_L1DDC_1_L1DDC_L1_s-pFE1-1_3m;3m;21638;
QL2C_Layer_1_pFEB_L1DDC_1_L1DDC_L1_s-pFE2_1.5m;1.5m;21650;
QL3C_Layer_1_pFEB_L1DDC_1_L1DDC_L1_s-pFE3_1m;1.0m;21658;
QL3C_Layer_4_sFEB_L1DDC_1_L1DDC_L4_s-pFE3_1m;1.0m;21665;
QL2C_Layer_4_sFEB_L1DDC_1_L1DDC_L4_s-pFE2_2m;2m;21657;
QL1C_Layer_4_sFEB_L1DDC_2_L1DDC_L4_s-pFE1-2_3m;3m;21649;
QL1C_Layer_4_sFEB_L1DDC_1_L1DDC_L4_s-pFE1-1_3m;3m;21648;
QL1C_Layer_3_pFEB_L1DDC_1_L1DDC_L3_s-pFE1-1_3m;3m;21640;
QL2C_Layer_3_pFEB_L1DDC_1_L1DDC_L3_s-pFE2_1.5m;1.5m;21652;
QL3C_Layer_3_pFEB_L1DDC_1_L1DDC_L3_s-pFE3_1m;1.0m;21660;
# On wedge Trigger Cables
XP-OUT12-LVD6R-2-2-1-QL1C-L3-sFEB_3;4.25m;21666;
XP-OUT13-LVD6R-2-2-2-QL2C-L3-sFEB_3;4.25m;21668;
QL1C_L3_sFEB_4_SR_Router2_J1;3m;21681;
QL2C_L3_sFEB_4_SR_Router2_J2;3m;21683;
XP-OUT14-LVD6R-2-2-3-QL3C-L3-sFEB_3;3m;21670;
QL3C_L3_sFEB_4_SR_Router2_J3;3m;21685;
XP-OUT18-LVD6R-2-1-1-QL1C-L1-sFEB_3;4.25m;21667;
XP-OUT19-LVD6R-2-1-2-QL2C-L1-sFEB_3;4.25m;21669;
QL1C_L1_sFEB_4_SR_Router1_J1;3m;21682;
QL2C_L1_sFEB_4_SR_Router1_J2;3m;21684;
XP-OUT20-LVD6R-2-1-3-QL3C-L1-sFEB_3;3m;21671;
QL3C_L1_sFEB_4_SR_Router1_J3;3m;21686;
XP-OUT21-LVD6R-3-1-1-QL1C-L2-sFEB_3;4.25m;21673;
XP-OUT22-LVD6R-3-1-2-QL2C-L2-sFEB_3;4.25m;21675;
QL1C_L2_sFEB_4_SR_Router5_J1;3m;21688;
QL2C_L2_sFEB_4_SR_Router5_J2;3m;21690;
XP-OUT23-LVD6R-3-1-3-QL3C-L2-sFEB_3;3m;21677;
QL3C_L2_sFEB_4_SR_Router5_J3;3m;21692;
XP-OUT15-LVD6R-3-2-1-QL1C-L4-sFEB_3;4.25m;21672;
XP-OUT16-LVD6R-3-2-2-QL2C-L4-sFEB_3;4.25m;21674;
QL1C_L4_sFEB_4_SR_Router6_J1;3m;21687;
QL2C_L4_sFEB_4_SR_Router6_J2;3m;21689;
XP-OUT17-LVD6R-3-2-3-QL3C-L4-sFEB_3;3m;21676;
QL3C_L4_sFEB_4_SR_Router6_J3;3m;21691;
pFEB_2_QL1C_L2L4_QL2C_L2L4_SRepeater_PT_IN0;2.5/1.5m breakout;21678;
pFEB_2_QL3C_L1L2L3L4_PT_IN3;4m breakout;21679;
pFEB_2_QL1C_L1L3_QL2C_L1L3_SRepeater_PT_IN5;2.5/1.5m breakout;21680;
# Off wedge Trigger Cables
pFEB_2_QL1C_L2L4_QL2C_L2L4_SRepeater_PT_IN0;3m;21705;20MNESR1RXXXXX
QL1C_L3_sFEB_4_SR_Router2_J1;3m;21707;20MNESR1RXXXXX
QL1C_L1_sFEB_4_SR_Router1_J1;3m;21708;20MNESR1RXXXXX
QL2C_L3_sFEB_4_SR_Router2_J2;3m;21709;20MNESR1RXXXXX
QL2C_L1_sFEB_4_SR_Router1_J2;3m;21710;20MNESR1RXXXXX
QL3C_L3_sFEB_4_SR_Router2_J3;3m;21711;20MNESR1RXXXXX
QL3C_L1_sFEB_4_SR_Router1_J3;3m;21712;20MNESR1RXXXXX
pFEB_2_QL1C_L1L3_QL2C_L1L3_SRepeater_PT_IN5;3m;21706;20MNESR1RXXXXX
QL1C_L4_sFEB_4_SR_Router6_J1;3m;21713;20MNESR1RXXXXX
QL1C_L2_sFEB_4_SR_Router5_J1;3m;21714;20MNESR1RXXXXX
QL2C_L4_sFEB_4_SR_Router6_J2;3m;21715;20MNESR1RXXXXX
QL2C_L2_sFEB_4_SR_Router5_J2;3m;21716;20MNESR1RXXXXX
QL3C_L4_sFEB_4_SR_Router6_J3;3m;21717;20MNESR1RXXXXX
QL3C_L2_sFEB_4_SR_Router5_J3;3m;21718;20MNESR1RXXXXX
XP-OUT12-LVD6R-2-2-1-QL1C-L3-sFEB_3;2m;21693;
XP-OUT18-LVD6R-2-1-1-QL1C-L1-sFEB_3;2m;21694;
XP-OUT13-LVD6R-2-2-2-QL2C-L3-sFEB_3;2m;21695;
XP-OUT19-LVD6R-2-1-2-QL2C-L1-sFEB_3;2m;21696;
XP-OUT14-LVD6R-2-2-3-QL3C-L3-sFEB_3;2m;21697;
XP-OUT20-LVD6R-2-1-3-QL3C-L1-sFEB_3;2m;21698;
XP-OUT15-LVD6R-3-2-1-QL1C-L4-sFEB_3;2m;21699;
XP-OUT21-LVD6R-3-1-1-QL1C-L2-sFEB_3;2m;21700;
XP-OUT16-LVD6R-3-2-2-QL2C-L4-sFEB_3;2m;21701;
XP-OUT22-LVD6R-3-1-2-QL2C-L2-sFEB_3;2m;21702;
XP-OUT17-LVD6R-3-2-3-QL3C-L4-sFEB_3;2m;21703;
XP-OUT23-LVD6R-3-1-3-QL3C-L2-sFEB_3;2m;21704;
