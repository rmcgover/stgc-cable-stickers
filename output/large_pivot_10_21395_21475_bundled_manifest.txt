
Cable;Length;SN;Repeater SN
# On wedge L1DDC Cables
QL3P_Layer_2_sFEB_L1DDC_1_L1DDC_L2_s-pFE3_1m;1.0m;21420;
QL2P_Layer_2_sFEB_L1DDC_1_L1DDC_L2_s-pFE2_2m;2m;21412;
QL1P_Layer_2_sFEB_L1DDC_2_L1DDC_L2_s-pFE1-2_3m;3m;21402;
QL1P_Layer_2_sFEB_L1DDC_1_L1DDC_L2_s-pFE1-1_3m;3m;21401;
QL1P_Layer_1_pFEB_L1DDC_1_L1DDC_L1_s-pFE1-1_3m;3m;21395;
QL2P_Layer_1_pFEB_L1DDC_1_L1DDC_L1_s-pFE2_1.5m;1.5m;21407;
QL3P_Layer_1_pFEB_L1DDC_1_L1DDC_L1_s-pFE3_1m;1.0m;21415;
QL3P_Layer_4_sFEB_L1DDC_1_L1DDC_L4_s-pFE3_1m;1.0m;21422;
QL2P_Layer_4_sFEB_L1DDC_1_L1DDC_L4_s-pFE2_2m;2m;21414;
QL1P_Layer_4_sFEB_L1DDC_2_L1DDC_L4_s-pFE1-2_3m;3m;21406;
QL1P_Layer_4_sFEB_L1DDC_1_L1DDC_L4_s-pFE1-1_3m;3m;21405;
QL1P_Layer_3_pFEB_L1DDC_1_L1DDC_L3_s-pFE1-1_3m;3m;21397;
QL2P_Layer_3_pFEB_L1DDC_1_L1DDC_L3_s-pFE2_1.5m;1.5m;21409;
QL3P_Layer_3_pFEB_L1DDC_1_L1DDC_L3_s-pFE3_1m;1.0m;21417;
QL3P_Layer_3_sFEB_L1DDC_1_L1DDC_L3_s-pFE3_1m;1.0m;21421;
QL2P_Layer_3_sFEB_L1DDC_1_L1DDC_L3_s-pFE2_2m;2m;21413;
QL1P_Layer_3_sFEB_L1DDC_2_L1DDC_L3_s-pFE1-2_3m;3m;21404;
QL1P_Layer_3_sFEB_L1DDC_1_L1DDC_L3_s-pFE1-1_3m;3m;21403;
QL1P_Layer_4_pFEB_L1DDC_1_L1DDC_L4_s-pFE1-1_3m;3m;21398;
QL2P_Layer_4_pFEB_L1DDC_1_L1DDC_L4_s-pFE2_1.5m;1.5m;21410;
QL3P_Layer_4_pFEB_L1DDC_1_L1DDC_L4_s-pFE3_1m;1.0m;21418;
QL3P_Layer_1_sFEB_L1DDC_1_L1DDC_L1_s-pFE3_1m;1.0m;21419;
QL2P_Layer_1_sFEB_L1DDC_1_L1DDC_L1_s-pFE2_2m;2m;21411;
QL1P_Layer_1_sFEB_L1DDC_2_L1DDC_L1_s-pFE1-2_3m;3m;21400;
QL1P_Layer_1_sFEB_L1DDC_1_L1DDC_L1_s-pFE1-1_3m;3m;21399;
QL1P_Layer_2_pFEB_L1DDC_1_L1DDC_L2_s-pFE1-1_3m;3m;21396;
QL2P_Layer_2_pFEB_L1DDC_1_L1DDC_L2_s-pFE2_1.5m;1.5m;21408;
QL3P_Layer_2_pFEB_L1DDC_1_L1DDC_L2_s-pFE3_1m;1.0m;21416;
# On wedge Trigger Cables
XP-OUT9-LVD6R-3-1-4-QL1P-L2-sFEB_3;4.25m;21430;
XP-OUT10-LVD6R-3-1-5-QL2P-L2-sFEB_3;4.25m;21432;
QL1P_L2_sFEB_4_SR_Router7_J1;3m;21445;
QL2P_L2_sFEB_4_SR_Router7_J2;3m;21447;
XP-OUT11-LVD6R-3-1-6-QL3P-L2-sFEB_3;3m;21434;
QL3P_L2_sFEB_4_SR_Router7_J3;3m;21449;
XP-OUT3-LVD6R-3-2-4-QL1P-L4-sFEB_3;4.25m;21429;
XP-OUT4-LVD6R-3-2-5-QL2P-L4-sFEB_3;4.25m;21431;
QL1P_L4_sFEB_4_SR_Router8_J1;3m;21444;
QL2P_L4_sFEB_4_SR_Router8_J2;3m;21446;
XP-OUT5-LVD6R-3-2-6-QL3P-L4-sFEB_3;3m;21433;
QL3P_L4_sFEB_4_SR_Router8_J3;3m;21448;
XP-OUT0-LVD6R-2-2-4-QL1P-L3-sFEB_3;4.25m;21423;
XP-OUT1-LVD6R-2-2-5-QL2P-L3-sFEB_3;4.25m;21425;
QL1P_L3_sFEB_4_SR_Router4_J1;3m;21438;
QL2P_L3_sFEB_4_SR_Router4_J2;3m;21440;
XP-OUT2-LVD6R-2-2-6-QL3P-L3-sFEB_3;3m;21427;
QL3P_L3_sFEB_4_SR_Router4_J3;3m;21442;
XP-OUT6-LVD6R-2-1-4-QL1P-L1-sFEB_3;4.25m;21424;
XP-OUT7-LVD6R-2-1-5-QL2P-L1-sFEB_3;4.25m;21426;
QL1P_L1_sFEB_4_SR_Router3_J1;3m;21439;
QL2P_L1_sFEB_4_SR_Router3_J2;3m;21441;
XP-OUT8-LVD6R-2-1-6-QL3P-L1-sFEB_3;3m;21428;
QL3P_L1_sFEB_4_SR_Router3_J3;3m;21443;
pFEB_2_QL1P_L2L4_QL2P_L2L4_SRepeater_PT_IN1;2.5/1.5m breakout;21435;
pFEB_2_QL3P_L1L2L3L4_PT_IN2;4m breakout;21436;
pFEB_2_QL1P_L1L3_QL2P_L1L3_SRepeater_PT_IN4;2.5/1.5m breakout;21437;
# Off wedge Trigger Cables
pFEB_2_QL1P_L1L3_QL2P_L1L3_SRepeater_PT_IN4;3m;21463;20MNESR1RXXXXX
QL1P_L4_sFEB_4_SR_Router8_J1;3m;21470;20MNESR1RXXXXX
QL1P_L2_sFEB_4_SR_Router7_J1;3m;21471;20MNESR1RXXXXX
QL2P_L4_sFEB_4_SR_Router8_J2;3m;21472;20MNESR1RXXXXX
QL2P_L2_sFEB_4_SR_Router7_J2;3m;21473;20MNESR1RXXXXX
QL3P_L4_sFEB_4_SR_Router8_J3;3m;21474;20MNESR1RXXXXX
QL3P_L2_sFEB_4_SR_Router7_J3;3m;21475;20MNESR1RXXXXX
pFEB_2_QL1P_L2L4_QL2P_L2L4_SRepeater_PT_IN1;3m;21462;20MNESR1RXXXXX
QL1P_L3_sFEB_4_SR_Router4_J1;3m;21464;20MNESR1RXXXXX
QL1P_L1_sFEB_4_SR_Router3_J1;3m;21465;20MNESR1RXXXXX
QL2P_L3_sFEB_4_SR_Router4_J2;3m;21466;20MNESR1RXXXXX
QL2P_L1_sFEB_4_SR_Router3_J2;3m;21467;20MNESR1RXXXXX
QL3P_L3_sFEB_4_SR_Router4_J3;3m;21468;20MNESR1RXXXXX
QL3P_L1_sFEB_4_SR_Router3_J3;3m;21469;20MNESR1RXXXXX
XP-OUT3-LVD6R-3-2-4-QL1P-L4-sFEB_3;2m;21456;
XP-OUT9-LVD6R-3-1-4-QL1P-L2-sFEB_3;2m;21457;
XP-OUT4-LVD6R-3-2-5-QL2P-L4-sFEB_3;2m;21458;
XP-OUT10-LVD6R-3-1-5-QL2P-L2-sFEB_3;2m;21459;
XP-OUT5-LVD6R-3-2-6-QL3P-L4-sFEB_3;2m;21460;
XP-OUT11-LVD6R-3-1-6-QL3P-L2-sFEB_3;2m;21461;
XP-OUT0-LVD6R-2-2-4-QL1P-L3-sFEB_3;2m;21450;
XP-OUT6-LVD6R-2-1-4-QL1P-L1-sFEB_3;2m;21451;
XP-OUT1-LVD6R-2-2-5-QL2P-L3-sFEB_3;2m;21452;
XP-OUT7-LVD6R-2-1-5-QL2P-L1-sFEB_3;2m;21453;
XP-OUT2-LVD6R-2-2-6-QL3P-L3-sFEB_3;2m;21454;
XP-OUT8-LVD6R-2-1-6-QL3P-L1-sFEB_3;2m;21455;
