# -*- coding: utf-8 -*-
"""Make a .stl file for each cable jig type"""

from typing import List
import subprocess


def make_jig(
    name: str,
    depths: List[int],
    file: str = 'cablejig_v0.scad'
) -> None:
    """Create cable jig file jig_<`name`>.stl with `depths` using `file`.
    Arguments:
        name -- the name of the cable  jig
    """
    command = [
        'openscad',
        '-o', f'jig_{name}.stl',
        '-D', f'depths={depths}',
        file,
    ]
    print(' '.join(command))
    subprocess.run(command, check=True)


jigs = {
    'large_wedge':
        [3, 2, 1, 0, 0, 2, 3],
    'small_wedge_14':
        [0, 2, 3, 0, 1, 2, 3],
    'small_wedge_23':
        [0, 1, 2, 3, 0, 2, 3],
    'generic':
        [2, 2, 2, 2, 2, 2, 2],
}

for jig, depths in jigs.items():
    make_jig(jig, depths)
