DEPTH_LEVELS = 4;
BLOCK_X = 15;
CABLE_X = 5;
GRID_X = BLOCK_X + CABLE_X;
GRID_Y = 30;
GRID_Z = 15;
BASE_Z = 1;
SIZE_X = BLOCK_X * 8 + CABLE_X * 7;
SIZE_Y = GRID_Y * DEPTH_LEVELS;
SIZE_Z = GRID_Z * 1;

module block(i, depths) {
    n = len(depths);
    size_x = BLOCK_X / n;
    translate([GRID_X * i, 0, 0]) {
        for (j = [0:n-1]) {
            depth = depths[j];
            trans_y = depth * GRID_Y;
            size_y = SIZE_Y - trans_y;
            translate([size_x * j, depth * GRID_Y, 0])
                cube([size_x, size_y, SIZE_Z]);
        }
    }
}

module cable(i, depth) {
    trans_y = depth * GRID_Y;
    size_y = SIZE_Y - trans_y;
    translate([BLOCK_X + GRID_X * i, trans_y, 0])
        cube([CABLE_X, size_y, BASE_Z]);
}

module jig(depths) {
    n = len(depths);
    assert(n > 0);
    blockargs = [
        for (i = 0; i <= n; i = i + 1)
        [i,
        (i == 0) ? [for (j = [1:3]) depths[i]] :
        (i == n) ? [for (j = [1:3]) depths[n - 1]] :
        [
            depths[i - 1],
            min(depths[i - 1], depths[i]),
            depths[i]
        ]
        ]
    ];
    //echo(blockargs);
    for (args = blockargs) block(args[0], args[1]);
    for (i = [0:n-1]) cable(i, depths[i]);
}

// Large Wedge L1DDC Bundles
//depths = [3, 2, 1, 0, 0, 2, 3];

// Small Wedge L1DDC Bundles 1 & 4
//depths = [0, 1, 2, 3, 0, 2, 3];

// Small Wedge L1DDC Bundles 2 & 3
//depths = [0, 2, 3, 0, 1, 2, 3];

// Generic Jig
depths = [2, 2, 2, 2, 2, 2, 2];

jig(depths);
