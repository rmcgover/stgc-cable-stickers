from __future__ import print_function

import argparse
import csv


def read_manifest(file):
    with file:
        reader = csv.reader(file, delimiter=';')
        rows = [row for row in reader]
    cables = []
    for row in rows[2:]:  # skip header
        if not row or row[0][0] == '#':
            continue
        cable = ';'.join(row[:3])
        cables.append(cable)
    return cables


def check_cables(cables=None):
    seen = set()
    n_cables = len(cables) if cables else -1
    while len(seen) != n_cables:
        if cables:
            print('\nCables scanned: {}/{}'.format(len(seen), n_cables))
        else:
            print('\nCables scanned: {}'.format(len(seen)))
        try:
            cable_left = input('Scan the left side of the cable: ')
        except KeyboardInterrupt:
            if not cables:
                res = input('\nExit? (y/n): ')
            else:
                print('The following cables have not been validated yet:')
                for cable in cables:
                    if cable not in seen:
                        print(cable)
                res = input('\nExit anyway? (y/n): ')
            if res.lower() == 'y':
                return
            else:
                continue
        if cable_left in seen:
            print('\aERROR: "{}" already scanned!'.format(cable_left))
            continue
        elif cables and cable_left not in cables:
            print('\aERROR: "{}" not in manifest!'.format(cable_left))
            continue
        try:
            cable_right = input('Scan the right side of the cable '
                                '(Ctrl-C to abort): ')
        except KeyboardInterrupt:
            print('\n')
            continue
        if cable_left != cable_right:
            print('\aERROR: "{}" does not match "{}"'.format(
                cable_left, cable_right))
        else:
            seen.add(cable_left)


def check_cables_inorder(cables):
    n_cables = len(cables)
    for i, cable in enumerate(cables):
        while True:
            print('\nCables scanned: {}/{}'.format(i, n_cables))
            print('Current cable: {}'.format(cable))
            cable_left = input('Scan the left side: ')
            if cable_left != cable:
                print('\aERROR: "{}" does not match'.format(cable_left))
                continue
            cable_right = input('Scan the right side: ')
            if cable_right != cable:
                print('\aERROR: "{}" does not match'.format(cable_right))
                continue
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--manifest', type=argparse.FileType(),
                        help='The cable bundle manifest to validate against')
    parser.add_argument('--inorder', action='store_true',
                        help='Check cables in manifest order')
    args = parser.parse_args()
    cables = read_manifest(args.manifest) if args.manifest else None
    if args.inorder and not cables:
        parser.error('--manifest is required to use --inorder')
    if not args.inorder:
        check_cables(cables)
    else:
        check_cables_inorder(cables)
