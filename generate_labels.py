#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 17:08:49 2019

@author: atlas-nsw-stgc-daq
"""

from typing import List, Optional, Union
import argparse as ap
from itertools import cycle
from os.path import join, dirname

from tqdm import tqdm

from reportlab.lib.units import mm
from reportlab.lib.pagesizes import A4
from reportlab.lib.colors import HexColor
from reportlab.pdfgen.canvas import Canvas
from reportlab.graphics.shapes import (
    Drawing, Line, Rect, String, STATE_DEFAULTS
)
from reportlab.graphics.barcode.qr import QrCodeWidget

from label_data import get_cables, bundle_cables, N_CABLES

# constants
NX = 2  # number of stickers per row
NY = 6  # number of stickers per column
X0 = 4.5 * mm           # left pos of first sticker
Y0 = A4[1] - 22.0 * mm  # top pos of first sticker
X1 = X0 + 99.1 * mm   # right pos of first sticker
Y1 = Y0 - 42.33 * mm  # bottom pos of first sticker
WIDTH = X1 - X0   # sticker width
HEIGHT = Y0 - Y1  # sticker height
DX = 101.5 * mm  # x-distance to next sticker
DY = 42.33 * mm  # y-distance to next sticker

WEDGE_TYPES = {
    'small_pivot':   'Small pivot',
    'small_confirm': 'Small confirm',
    'large_pivot':   'Large pivot',
    'large_confirm': 'Large confirm',
}

# debug flag
_DEBUG = False


def wrap(par: str, width: int, sep: Optional[str] = ' -_') -> List[str]:
    '''Wrap the paragraph `par` to `width`, breaking on separators in `sep`
    if possible
    '''
    lines = []
    seps = list(sep) if sep is not None else []
    while len(par) > width:
        # find separators
        finds = []
        for sep in seps:
            find = par.rfind(sep, 0, width)
            if find != -1:
                finds.append(find)
        # break at width if no sep found, otherwise furthest sep
        i = max([f + 1 for f in finds], default=width)
        lines.append(par[:i])
        par = par[i:]
        pass
    lines.append(par)
    return lines


def get_qr_content(name: str, length: str, serial: Union[int, str]) -> str:
    return ';'.join((name, length, str(serial)))


def get_text_content(name: str, length: str, serial: Union[int, str]) -> str:
    return '\n'.join(wrap(name, 30) + [f'{length} SN: {serial}'])


def make_title(c: Canvas, s: str) -> None:
    '''Place a title at the top of the page'''
    width = A4[0]
    height = A4[1] - Y0
    d = Drawing(width, height)
    d.add(String(width / 2, height / 3, s, textAnchor='middle', fontSize=18))
    d.drawOn(c, 0, Y0)


def draw_sticker_pair(c: Canvas, n: int, cable: dict) -> None:
    '''Draw a pair of stickers on the canvas at position `n`'''
    if n >= NY:
        raise ValueError(f'Sticker {n} flows off the page')

    qr_content = cable['QR Content']
    text_content = cable['Text Content']
    cable_type_color = cable.get('Cable Type Color', None)
    cable_layer_colors = cable.get('Cable Layer Colors', [])
    cable_latch_dir = cable.get('Latch Direction', '')

    # split the stickers in half
    stick = Drawing(WIDTH, HEIGHT)
    stick.add(Line(0, HEIGHT / 2, WIDTH, HEIGHT / 2, strokeDashArray=[2, 2]))
    if _DEBUG:
        # draw debug sticker boundary
        stick.add(Rect(0, 0, WIDTH, HEIGHT, fillOpacity=0))
    for j in range(2):
        stick.drawOn(c, X0 + j * DX, Y1 - n * DY)

    # half-stickers geometry
    pad = 3.0 * mm
    width, height = WIDTH - 2 * pad, (HEIGHT / 2) - 2 * pad
    hstick_b = Drawing(width, height)  # bottom half
    hstick_t = Drawing(width, height)  # top half
    # qr codes
    qrwidth, qrheight = height, height
    hstick_b.add(QrCodeWidget(qr_content, x=0, y=0,
                              barWidth=qrwidth, barHeight=qrheight,
                              barBorder=0))
    hstick_t.add(QrCodeWidget(qr_content, x=width - height, y=0,
                              barWidth=qrwidth, barHeight=qrheight,
                              barBorder=0))
    # colorbars
    barwidth, barheight = 5 * mm + pad, height / 2
    barstroke = 0.5  # pt
    if cable_type_color is not None:
        hstick_b.add(Rect(width + pad - barwidth, barheight,
                          barwidth, barheight,
                          strokeWidth=barstroke, fillColor=cable_type_color))
        hstick_t.add(Rect(-pad, barheight,
                          barwidth, barheight,
                          strokeWidth=barstroke, fillColor=cable_type_color))
    nlayers = len(cable_layer_colors)
    layerheight = (height / 2) / nlayers if nlayers else 0
    for i, color in enumerate(cable_layer_colors):
        if color is None:
            continue
        y = barheight - layerheight * (nlayers - i)
        hstick_b.add(Rect(width + pad - barwidth, y, barwidth, layerheight,
                          strokeWidth=barstroke, fillColor=color))
        hstick_t.add(Rect(-pad, y, barwidth, layerheight,
                          strokeWidth=barstroke, fillColor=color))
    # remove colorbar if not there, or push measurement back in bounds
    if (cable_type_color is None
            and all(color is None for color in cable_layer_colors)):
        barwidth = 0
    else:
        barwidth -= pad
    # text
    lines = text_content.split('\n')
    textwidth = width - qrwidth - barwidth
    for i, line in enumerate(lines):
        yi = height - (i + 1) * height / (len(lines) + 1)
        hstick_b.add(String(qrwidth + textwidth / 2,  yi, line,
                            textAnchor='middle'))
        hstick_t.add(String(barwidth + textwidth / 2, yi, line,
                            textAnchor='middle'))
    if _DEBUG:
        # draw debug half-sticker boundary
        hstick_b.add(Rect(0, 0, width, height, fillOpacity=0))
        hstick_t.add(Rect(0, 0, width, height, fillOpacity=0))
    if cable_latch_dir == 'Up':
        # move colorbar so it is aligned with side visible at FEB insertion
        hstick_b, hstick_t = hstick_t, hstick_b
    # duplicate sticker across row
    for j in range(NX):
        hstick_b.drawOn(c, X0 + j * DX + pad, Y1 - n * DY + pad)
        hstick_t.drawOn(c, X0 + j * DX + pad, Y1 - n * DY + pad + HEIGHT / 2)


def draw_marker(c: Canvas, n: int) -> None:
    '''Draw a marker in the center column of the canvas at position `n`'''
    if n >= NY:
        raise ValueError('Position {n} flows off the page')
    center = X1 + (DX - WIDTH) / 2
    width = 1 * mm
    x = center - width / 2
    y = Y1 - n * DY + HEIGHT * (3 / 8)
    height = HEIGHT / 4
    c.rect(x, y, width, height, stroke=False, fill=True)


def make_group_pages(c: Canvas,
                     title: str,
                     cables: List[dict],
                     marker_idxs: List[int]
                     ) -> None:
    '''Given data for stickers, prints them to consecutive pages'''
    first = True
    num_pages = (len(cables) + NY - 1) // NY
    for i, cable in tqdm(enumerate(cables), desc=title, unit=' stickers'):
        if i % NY == 0:
            if not first:
                c.showPage()
            page_num = i // NY + 1
            make_title(c, f'{title} ({page_num}/{num_pages})')
        draw_sticker_pair(c, i % NY, cable)
        if i in marker_idxs:
            draw_marker(c, i % NY)
        first = False


# https://yagisanatode.com/2019/08/06/google-apps-script-hexadecimal-color-codes-for-google-docs-sheets-and-slides-standart-palette/
CABLE_TYPE_COLORS = {
    'L1DDC 1': HexColor(0xEFEFEF),  # light gray 2
    'L1DDC 2': HexColor(0xFF0000),  # red
    'Router': HexColor(0x3C78D8),  # dark cornflower blue 1
    'Pad Trigger/sFEB': HexColor(0xFFE000),
    'Pad Trigger/pFEB': HexColor(0xFF8000),
}

CABLE_LAYER_COLORS = {
    1: HexColor(0xFFB3CF),
    2: HexColor(0x783F04),  # dark orange 3
    3: HexColor(0xBF9000),  # dark yellow 2
    4: HexColor(0x6C1C80),
}


def make_wedge(wedge_type: str, seq_num: int, start_num: int, bundle: bool,
               nocolor: bool) -> None:
    '''Creates a file containing the stickers for a single wedge'''
    end_num = start_num + N_CABLES[wedge_type] - 1
    filehead = f'{wedge_type}_{seq_num}_{start_num}_{end_num}' \
        + ('_bundled' if bundle else '') \
        + ('_nocolor' if nocolor else '') \
        + ('_debug' if _DEBUG else '')
    stickerpath = join(dirname(__file__), 'output', filehead + '.pdf')
    manifestpath = join(dirname(__file__), 'output',
                        filehead + '_manifest.txt')
    print(f'Making stickersheet {filehead}...')
    c = Canvas(stickerpath, pagesize=A4)
    m = open(manifestpath, 'w')
    m.write('\n')
    m.write('Cable;Length;SN;Repeater SN\n')

    title = WEDGE_TYPES[wedge_type]
    c.setTitle(f'{title} {start_num}-{end_num}')
    # because we draw text with String rather than Canvas, we modify
    # the module defaults before drawing
    STATE_DEFAULTS['fontName'] = 'Helvetica'
    STATE_DEFAULTS['fontSize'] = 10

    cable_groups = get_cables(wedge_type)

    # first pass: attach serial numbers, qr and text content, and colors
    i = start_num
    for cable_group, df in cable_groups.items():
        # attach serial numbers
        df['Serial Number'] = [i + j for j in range(len(df))]
        if 'Start Point' in df.columns:
            df['Repeater Serial Number'] = [
                '20MNESR1RXXXXX' if s == 'Serial Repeater' else ''
                for s in df['Start Point']
            ]
        else:
            df['Repeater Serial Number'] = ['' for _ in range(len(df))]
        i += len(df)
        # attach content
        df['QR Content'] = df.apply(
            lambda row: get_qr_content(
                row['Label Name'], row['Length'], row['Serial Number']),
            axis=1
        )
        df['Text Content'] = df.apply(
            lambda row: get_text_content(
                row['Label Name'], row['Length'], row['Serial Number']),
            axis=1
        )
        # attach colors
        if nocolor:
            continue
        df['Cable Type Color'] = df['Cable Type'].map(
            lambda cable_type: CABLE_TYPE_COLORS[cable_type]
        )
        df['Cable Layer Colors'] = df['Layer'].map(
            lambda layers: [CABLE_LAYER_COLORS[l] for l in layers]
        )

    # potentially permute rows into bundles; get bundle start indices
    if bundle:
        cable_bundle_idxs = bundle_cables(cable_groups)
    else:
        cable_bundle_idxs = {cable_group: [] for cable_group in cable_groups}

    # second pass: print manifest, stickers
    first = True
    for cable_group, df in cable_groups.items():
        if not first:
            # make new page
            c.showPage()
        pagetitle = f'{title}: {cable_group}'
        m.write(f'# {cable_group}\n')
        cables = df.to_dict('records')
        marker_idxs = cable_bundle_idxs[cable_group]
        for cable in cables:
            qr_content = cable['QR Content']
            r_serial = cable['Repeater Serial Number']
            m.write(qr_content + ';' + r_serial + '\n')
        make_group_pages(c, pagetitle, cables, marker_idxs)
        first = False

    c.save()
    m.close()
    print('Done.')


def make_custom() -> None:
    # read custom stickers
    custompath = join(dirname(__file__), 'custom.txt')
    with open(custompath) as f:
        lines = [l.rstrip('\n') for l in f.readlines()]
    if len(lines) % 2 != 0:
        assert lines.pop() == ''
    indices = [int(n) for n in lines[::2]]
    newpage = [False] + [j <= i for i, j in zip(indices[:-1], indices[1:])]
    qr_contents = lines[1::2]
    text_contents = [get_text_content(*qr.split(';')) for qr in qr_contents]
    cables = [{'QR Content': q, 'Text Content': t}
              for q, t in zip(qr_contents, text_contents)]

    # make sheet
    filehead = 'custom_stickers'
    stickerpath = join(dirname(__file__), 'output', filehead + '.pdf')
    print(f'Making stickersheet {filehead}...')
    c = Canvas(stickerpath, pagesize=A4)

    title = 'Custom stickersheet'
    c.setTitle(title)

    STATE_DEFAULTS['fontName'] = 'Helvetica'
    STATE_DEFAULTS['fontSize'] = 10

    for i in tqdm(range(len(indices)), desc='Custom stickersheet',
                  unit=' stickers'):
        if newpage[i]:
            c.showPage()
        draw_sticker_pair(c, indices[i], cables[i])
    c.save()
    print('Done.')


if __name__ == '__main__':
    def main(in_args: Optional[List[str]] = None) -> None:
        # TODO: subcommands for single, multiple, replacement
        parser = ap.ArgumentParser(
            description='Print cable labels for sTGC wedges')
        single_choices = tuple(WEDGE_TYPES.keys())
        multiple_choices = ('small', 'large', 'all')
        choices = single_choices + multiple_choices + ('custom',)
        parser.add_argument('--wedge-type', choices=choices, required=True,
                            help='The type of wedge to generate labels for')
        parser.add_argument('--start-num', type=int,
                            help='The starting cable serial number')
        parser.add_argument('--side', choices=('a', 'c'), default='ac',
                            help='Which wheel (Side A or Side C)')
        parser.add_argument('--nobundle', action='store_false', dest='bundle',
                            help='Don\'t output stickers in bundle order')
        parser.add_argument('--nocolor', action='store_true',
                            help='Print the stickers without colorbars')
        parser.add_argument('--debug', action='store_true',
                            help=ap.SUPPRESS)
        args = parser.parse_args(in_args)

        if args.start_num is None and args.wedge_type != 'custom':
            parser.error('the following arguments are required: start_num')

        global _DEBUG
        _DEBUG = args.debug

        wedges = []
        if args.wedge_type in multiple_choices:
            if args.side == 'ac' and args.wedge_type != 'all':
                msg = '--side must be specified if wedge-type is not "all"'
                parser.error(msg)
            # a side
            if args.wedge_type in ('small', 'all') and 'a' in args.side:
                n = N_CABLES['small_pivot']
                # first sector has exceptional numbering, start with 2nd
                wedges += [
                    ('small_' + t, 2 + i // 2, args.start_num + n * i)
                    for t, i in zip(cycle(('pivot', 'confirm')), range(14))
                ]
                args.start_num += n * 14
            if args.wedge_type in ('large', 'all') and 'a' in args.side:
                n = N_CABLES['large_pivot']
                wedges += [
                    ('large_' + t, 1 + i // 2, args.start_num + n * i)
                    for t, i in zip(cycle(('pivot', 'confirm')), range(16))
                ]
                args.start_num += n * 16
            # c side
            if args.wedge_type in ('small', 'all') and 'c' in args.side:
                n = N_CABLES['small_pivot']
                wedges += [
                    ('small_' + t, 9 + i // 2, args.start_num + n * i)
                    for t, i in zip(cycle(('pivot', 'confirm')), range(16))
                ]
                args.start_num += n * 16
            if args.wedge_type in ('large', 'all') and 'c' in args.side:
                n = N_CABLES['large_pivot']
                wedges += [
                    ('large_' + t, 9 + i // 2, args.start_num + n * i)
                    for t, i in zip(cycle(('pivot', 'confirm')), range(16))
                ]
                args.start_num += n * 16
        elif args.wedge_type in single_choices:
            wedges = [(args.wedge_type, 0, args.start_num)]
        else:
            make_custom()
            return
        for wedge_type, seq_num, start_num in wedges:
            make_wedge(wedge_type, seq_num, start_num,
                       args.bundle, args.nocolor)

    main()
