#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 17:15:06 2019

@author: atlas-nsw-stgc-daq
"""

from typing import Dict, List
from os.path import join, dirname

import pandas as pd
import numpy as np

# number of cables per wedge type
N_CABLES = {
    'small_pivot':   77,
    'small_confirm': 77,
    'large_pivot':   81,
    'large_confirm': 81,
}

_CABLES_MEMO = {}


def _cables_copy(cables: Dict[str, pd.DataFrame]):
    return {g: df.copy() for g, df in cables.items()}


def _fixup_int(string):
    try:
        return int(string)
    except ValueError:
        return None


def get_cables(wedge_type: str) -> Dict[str, pd.DataFrame]:
    '''Get a dict of cables by group from the file corresponding to
    `wedge_type`
    '''
    # memoize for multiple generation
    if wedge_type in _CABLES_MEMO:
        return _cables_copy(_CABLES_MEMO[wedge_type])

    df = pd.read_csv(join(dirname(__file__), 'data',
                          f'{wedge_type}.csv'),
                     header=None)
    # get cable groups
    table_names = [
        'On wedge L1DDC Cables',
        'On wedge Pad Trigger Cables',
        'On wedge Router Trigger Cables',
        'Off wedge Pad Trigger Cables',
        'Off wedge Router Trigger Cables',
    ]
    groups = df[0].isin(table_names).cumsum()  # bool sum => increasing index
    cables = {g.iloc[0, 0]: g.iloc[1:] for _, g in df.groupby(groups)
              if g.iloc[0, 0] in table_names}
    # munge
    for g in cables:
        # correct header
        cables[g].columns = cables[g].iloc[0]
        cables[g] = cables[g].iloc[1:]
        # drop empty rows, cols
        cables[g] = cables[g].dropna(axis=0, how='all')
        cables[g] = cables[g].dropna(axis=1, how='all')
        # fixup name
        cables[g]['Label Name'] = cables[g]['Label Name'].map(
            lambda s: s.replace(' ', ''))
        # fixup length
        cables[g]['Length'] = cables[g]['Length'].map(
            lambda s: s.replace(' Cable', '').replace(' cable', ''))
        # bundling sorts wrong (string lexicographic) if we don't do this
        if 'Bundle Number' in cables[g].columns:
            cables[g]['Bundle Number'] = cables[g]['Bundle Number']\
                .map(_fixup_int)
            if 'Bundle Index' in cables[g].columns:
                cables[g]['Bundle Index'] = cables[g]['Bundle Index']\
                    .map(_fixup_int)
        # add Layer Number
        layersrc = 'Detector Layer' if 'Detector Layer' in cables[g].columns\
            else 'Original Detector Layer'
        cables[g]['Layer'] = cables[g][layersrc].map(
            lambda s: [int(l[-1]) for l in s.split(', ')])

    if sum(len(g) for g in cables.values()) != N_CABLES[wedge_type]:
        raise ValueError('Table misparse in {wedge_type}.csv')

    _CABLES_MEMO[wedge_type] = cables
    return _cables_copy(cables)


def bundle_cables(cables: Dict[str, pd.DataFrame]) -> Dict[str, List[int]]:
    '''Sorts `cables` in-place by Bundle Number and Bundle Index
    (if available); returns a dict of bundle start indices by group
    NB: This merges trigger cables into on-wedge and off-wedge groups
    '''
    # merge cable groups bundled together
    cables['On wedge Trigger Cables'] \
        = pd.concat([cables['On wedge Pad Trigger Cables'],
                     cables['On wedge Router Trigger Cables']])
    del cables['On wedge Pad Trigger Cables']
    del cables['On wedge Router Trigger Cables']
    cables['Off wedge Trigger Cables'] \
        = pd.concat([cables['Off wedge Pad Trigger Cables'],
                     cables['Off wedge Router Trigger Cables']])
    del cables['Off wedge Pad Trigger Cables']
    del cables['Off wedge Router Trigger Cables']
    # sort into bundles
    for g in cables:
        if 'Bundle Number' in cables[g].columns:
            if 'Bundle Index' in cables[g].columns:
                cables[g] = cables[g].sort_values(
                    by=['Bundle Number', 'Bundle Index'], kind='mergesort')
            else:
                cables[g] = cables[g].sort_values(
                    by=['Bundle Number'], kind='mergesort')
    # get bundle start indices
    start_idxs = {}
    for g in cables:
        bundle_diffs = cables[g]['Bundle Number'].astype(float).diff()
        diff_idxs = np.where(bundle_diffs.values == 1)[0]
        start_idxs[g] = [0] + list(diff_idxs)
    return start_idxs


if __name__ == '__main__':
    cables = get_cables('large_confirm')
    bundle_start_idxs = bundle_cables(cables)
