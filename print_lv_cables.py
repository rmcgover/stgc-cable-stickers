import argparse

from my_dymoprint import print_label


FSR = 0.4

LABELS = tuple(
    # 9 spaces is about a centimeter at this font size
    f'Q{q} L{l} {k}FEB            Q{q} L{l} {k}FEB'
    for ks in ('spsp', 'psps')
    for q in range(1, 3 + 1)
    for l, k in zip(range(1, 4 + 1), ks)
)


def print_labels(labels, index=0, _dry_run=False):
    try:
        for i, label in list(enumerate(labels))[index:]:
            print(i, label)
            if _dry_run:
                continue
            print_label(label, fontsizeratio=FSR)
            print_label(label, fontsizeratio=FSR)
    except KeyboardInterrupt:
        print()


if __name__ == "__main__":
    def main():
        parser = argparse.ArgumentParser()
        parser.add_argument('offset', type=int, nargs='?', default=0,
                            help='Which label index to start at')
        parser.add_argument('--dry-run', action='store_true',
                            help=argparse.SUPPRESS)
        args = parser.parse_args()

        print_labels(LABELS, args.offset, args.dry_run)

    main()
