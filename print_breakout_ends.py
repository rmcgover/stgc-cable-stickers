import argparse

from my_dymoprint import print_label


def ends(size, wedge_type):
    s = size
    w = wedge_type
    # labels are 440mm long
    # w/ 7m cartridge, that gives 15 wedges per cartridge
    return (
        *(f'P0_Q{s}2{w}_L2_1.5m', f'P1_Q{s}2{w}_L4_1.5m',
          f'P2_Q{s}1{w}_L2_2.5m', f'P3_Q{s}1{w}_L4_2.5m',),
        *(f'P0_Q{s}3{w}_L1_4m', f'P1_Q{s}3{w}_L2_4m',
          f'P2_Q{s}3{w}_L3_4m', f'P3_Q{s}3{w}_L4_4m',),
        *(f'P0_Q{s}2{w}_L1_1.5m', f'P1_Q{s}2{w}_L3_1.5m',
          f'P2_Q{s}1{w}_L1_2.5m', f'P3_Q{s}1{w}_L3_2.5m',),
    )


def print_labels(labels, index=0, _dry_run=False):
    try:
        for i, label in list(enumerate(labels))[index:]:
            print(i, label)
            if _dry_run:
                continue
            print_label(label, '', label)
    except KeyboardInterrupt:
        print()


if __name__ == "__main__":
    def main():
        parser = argparse.ArgumentParser()
        parser.add_argument('small_or_large', choices=('small', 'large'),
                            help='Small or large wedge')
        parser.add_argument('pivot_or_confirm', choices=('pivot', 'confirm'),
                            help='Pivot or confirm wedge')
        parser.add_argument('offset', type=int, nargs='?', default=0,
                            help='Which label index to start at')
        parser.add_argument('--dry-run', action='store_true',
                            help=argparse.SUPPRESS)
        args = parser.parse_args()

        size = 'S' if args.small_or_large == 'small' else 'L'
        wedge_type = 'P' if args.pivot_or_confirm == 'pivot' else 'C'

        print_labels(ends(size, wedge_type), args.offset, args.dry_run)

    main()
