# NSW Electronics - Cable labeling for sTGC electronics

## Table of contents

[[_TOC_]]

## Introduction

This project collects the scripts used to generate labels for the mini-SAS and low-voltage power (LV) cables for sTGC wedge electronics.

These scripts are designed for [Avery Zweckform A4 sticker sheets](https://www.avery-zweckform.com/blanko-etiketten/rechteckig-99x42-mm) and a [Dymo LabelManager PnP](https://www.avery-zweckform.com/blanko-etiketten/rechteckig-99x42-mm) label printer.

* mini-SAS cables:
    * [`generate_labels.py`](generate_labels.py): Create stickersheets based on the data in the `data/` directory
    * [`print_breakout_ends.py`](print_breakout_ends.py): Use the DYMO label printer to print labels for the ends of the 1-to-4 mini-SAS breakout cables
    * [`cable_check.py`](cable_check.py): Check that stickers attached to both ends of a cable are the same, used for validating mini-SAS cable bundles
* LV cables:
    * [`print_lv_cables.py`](print_lv_cables.py): Use the DYMO label printer to print labels for the FEB LV cables
    * [`print_lv_cables_l1ddc.py`](print_lv_cables_l1ddc.py): Use the DYMO label printer to print labels for the L1DDC and SR LV cables
* General:
    * [`my_dymoprint.py`](my_dymoprint.py): Use the DYMO label printer to print lines from the command line

Additionally, code to produce the mini-SAS L1DDC cable bundle jigs is included in the [`cablejig/`](cablejig/) directory.

## Dependencies

Utilities which use the DYMO label printer require the following Python packages:

* PIL/`pillow`, an image processing library
* PyUSB, a USB interface library

`generate_labels.py` requires:

* `pandas` and `numpy`, to process the input sticker data
* `reportlab`, to create QR codes and output PDF files
* `tqdm`, to make waiting for stickersheets to be generated less agonizing

Dependencies can be acquired by creating a new [Anaconda](https://www.anaconda.com/) environment from the [`environment.yml`](environment.yml) file:
```bash
conda env create -n <env-name> -f environment.yml
conda activate <env-name>
```
or merging with an existing environment:
```bash
conda env update -n <env-name> -f environment.yml
conda activate <env-name>
```

## [`generate_labels.py`](generate_labels.py)

Invoke with `--help` for usage. Example usage:
```bash
# Generate cable labels, in bundle order, for the A-side small wedges
python generate_labels.py --wedge-type small --start-num 10001
```

[Siyuan Sun](mailto:Siyuan.Sun@cern.ch), [Bobby McGovern](mailto:robert.mcgovern@cern.ch), and [Marius Kongsore](mailto:marius.kongsore@cern.ch) contributed to the cable label format and bundling plan. For further bundling documentation, see the [ATLAS TWiki](https://twiki.cern.ch/twiki/bin/view/Atlas/NSWSTGCWedgeELTXIntegration#Preparation_of_TwinAx_cable_bund).

[Liang Guan](mailto:Liang.Guan@cern.ch) has specified that cables will be made distinct by giving each cable a unique serial number. When generating cable labels, be sure that the `start_num` argument you specify is distinct from previous cables.

In the case of a hopelessly mangled single label/label pair, edit the `custom.txt` file at the root directory to take the form
```
<sticker 1 index>
<sticker 1 QR code content>
<sticker 2 index>
<sticker 2 QR code content>
...

```
where the "sticker index" is a number from 0-5 specifying the row on which to print the sticker (non-increasing indices force a page-break). Then, run the script with the `custom` wedge type:
```bash
python generate_labels.py --wedge-type custom
```

## [`print_breakout_ends.py`](print_breakout_ends.py)

Invoke at the command line with the wedge size and type, and an optional offset into the list of labels:
```bash
python print_breakout_ends.py {small,large} {pivot,confirm} [offset]
```

## [`cable_check.py`](cable_check.py)

This is a script to check that stickers attached to either end of a cable are the same, using a QR code scanner.

With the `--manifest` argument, it can also ensure that all stickers from a given manifest are attached.

With the `--inorder` argument, it will require that stickers are scanned in manifest order.

The suggested command-line usage is
```bash
python cable_check.py --manifest ./output/<cable bundle manifest>
```

Scanning can be exited by pressing `ctrl-C`; in manifest mode, this will display a list of unscanned cables first before asking to exit.

## [`print_lv_cables.py`](print_lv_cables.py)

Invoke at the command line with an optional offset into the list of labels:
```bash
python print_lv_cables.py [offset]
```

Because this command prints a significant number of stickers, it is very possible that the label cartridge will run out of label material before the command completes. In this case, look at the last label that was printed, and compare with the command line to find the index of the last label printed. Change the label cartridge, and re-invoke the script with this offset index to begin printing from there.


## [`print_lv_cables_l1ddc.py`](print_lv_cables_l1ddc.py)

Invoke this at the command line with the sector number, and the wedge size and type:
```bash
# Example usage
python print_lv_cables_l1ddc.py 8 small confirm
```

Because this command prints a significant number of stickers, it is very possible that the label cartridge will run out of label material before the command completes. In this case, look at the last label that was printed, and compare with the command line to find the index of the last label printed. Change the label cartridge, and re-invoke the script with this offset index to begin printing from there:
```bash
python print_lv_cables_l1ddc.py 8 small confirm <offset>
```

## [`my_dymoprint.py`](my_dymoprint.py)

Invoke with `--help` for usage. Use this script if you want to print an individual label with the DYMO label printer; don't be confused by the `lines` argument, those lines are stacked on top of each other vertically.

```bash
# Example usage
python my_dymoprint.py 'Hello, world!' 'Row 2 of label' --fonstizeratio 0.5
```

## Issues with the Dymo label printer

Because the Dymo printer is designed for use with Windows, controlling it via the `my_dymoprint.py` script on Linux is finicky. If mysterious errors from libusb are emitted, try turning the printer off and then back on again.
